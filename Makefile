CC = g++
CFLAGS = -c
TARGETS := dna_strand

all: dna_strand

unittest: $(TARGETS)
	rm -f valgrind.log
	-valgrind --leak-check=full --show-leak-kinds=all --track-fds=yes --log-file=valgrind.log ./dna_strand
	cat valgrind.log

dna_strand: main.o Strand.o
	$(CC) main.cpp Strand.cpp -o dna_strand

main.o: main.cpp
	$(CC) $(CFLAGS) main.cpp

Strand.o: Strand.cpp
	$(CC) $(CFLAGS) Strand.cpp

clean:
	rm -rf *o main
	rm -rf dna_strand

spotless: clean
	-rm -f $(TARGETS)
