#include "Strand.h"
#include <iostream>

int main(int argc, char const *argv[]) {
  char strand[16] = "AACGTCAGTACGGTA";
  Strand s(strand);

  std::cout << s.getStrand() << std::endl;

  return 0;
}
