#include "Strand.h"

#include <cstring>

// CONSTRUCTORS //

Strand::Strand()
: mStrand(0)
{
}

Strand::Strand(const Strand &src)
{
  *this = src;
}

Strand::Strand(char *strand)
: mStrand(0)
{
  setStrand(strand);
}

Strand::~Strand()
{
  if (mStrand) {
    delete [] mStrand;
  }
}

// CLASS METHODS //

/*

Strand Strand::substrand(size_t i, size_t j) const
{

}

Strand Strand::merge(size_t i, const Strand &right) const
{

}

size_t Strand::overlap(const Strand &right) const
{

}

Strand Strand::operator==(const Strand &rhs)
{

}

Strand Strand::operator+=(const Strand &rhs)
{

}

Strand &Strand::operator=(const Strand &rhs)
{

}
*/

bool Strand::setStrand(const char *strand)
{
  if (mStrand) {
    delete[] mStrand;
    mStrand = 0;
  }

  if (strand) {
    mStrand = new char[std::strlen(strand) + 1];
    std::strcpy(mStrand, strand);
    return true;
  }
  else {
    return false;
  }
}

const char * Strand::getStrand() const
{
  return mStrand;
}
