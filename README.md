# DNA Sequencing

The Human Genome Project  was a landmark 13 year project to map the human DNA. The project made it possible for many advances medicine. It is also useful for other fields, such as agriculture and anthropology.

The basic process used was to extract the DNA from the cells of a person. Unfortunately, the whole DNA can’t be extracted in one piece. It is usually broken up into a very large number of small pieces. The small pieces are processed in a sequencer machine capable of detecting the strand of molecules. The result is processing one strand then is a sequence of letters describing the type and order of molecules in the strand. The strands only contain four bases: adenine (A), guanine (G), cytosine (C), and thymine (T). So, strands are represented by a text string containing the letters AGCT.

In order to recreate the order of the full DNA strand, these pieces must be stitched together in the correct order.
To do this, the ends of two incomplete DNA strands are compared to see if they match when overlapped. If so, it is possible that these DNA strands are two pieces of a larger whole. The strands are matched and merged repeatedly until the full DNA string is reproduced.

## To Build

    git clone git@gitlab.com:nicholasrucci/DNA.git
    make

## Runnning the Program

    ./dna_strand

## Running Valgrind

    make unittest

## To Clean

    make clean
