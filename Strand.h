#ifndef _STRAND_H_
#define  _STRAND_H_

#include <cstdlib>

class Strand
{
public:
    Strand();
    Strand(const Strand &src);
    Strand(char strand[]);
    ~Strand();

    const char * getStrand() const;
    bool setStrand(const char strand[]);
    // Strand substrand(size_t i, size_t j) const;
    // Strand merge(size_t i, const Strand &right) const;
    // size_t overlap(const Strand &right) const;
    //
    // Strand operator==(const Strand &rhs);
    // Strand operator+=(const Strand &rhs);
    // Strand & operator=(const Strand &rhs);

private:
  char *mStrand;

};

#endif /* _STRAND_H_ */
